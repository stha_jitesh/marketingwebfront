/**
 * Created by jitesh on 4/17/17.
 */
var requestHeaders = {"Accept": "application/json", "Content-Type": "application/json"};

function request(url, request_type, datatype, callback, postdata) {

    // For debug only
    // var SERVER = "http://13.56.52.215:8080";
    //var SERVER = "http://54.193.70.29:8080";
    /*"http://52.27.46.230:8080";*/
    var SERVER = "http://localhost:8080";
    url = SERVER + url;
    console.log(url);
    if (typeof(postdata) == "object") {
        postdata = JSON.stringify(postdata);

    }

    $.ajax({
        beforeSend: function (xhr) {
            for (var key in requestHeaders) {
                xhr.setRequestHeader(key, requestHeaders[key]);
                ajaxindicatorstart("...Please wait.. Data is loading...");
            }
        },
        url: url,
        xhrFields: {
            withCredentials: true
        },
        data: postdata,
        dataType: datatype,
        type: request_type,
        success: function (data) {
            ajaxindicatorstop();
            callback("success", data);
        },
        error: function (xhr, status, error) {
            ajaxindicatorstop();
            try {
                var errorMessageObj = JSON.parse(xhr.responseText);
            } catch (err) {
                errorMessageObj = {type: "ERROR", message: xhr.responseText};
            }
            callback(error, errorMessageObj);
        }
    });
}