/**
 * Created by satish on 6/1/17.
 */

function tabPaneItems(){
    $("#tab_name").html("Desktop");
    $("#desktop").removeClass("hidden");
    $('#items-tab a').tab('show');
    $('#items-tab a').click(function (e) {
        e.preventDefault();
        $(this).tab('show');
        var selector = $(this).attr('href');
        if (selector == "#accessories") {
            $("#accessories").removeClass("hidden");
            $("#tab_name").html("Computer Accessories");
            $("#acc").addClass("active");
            $("#desk").removeClass("active");
            $("#dev").removeClass("active");
            $("#mon").removeClass("active");
            $("#net").removeClass("active");
            $("#print").removeClass("active");
            $("#game").removeClass("active");
            $("#soft").removeClass("active");
            $("#lap").removeClass("active");
        } else if (selector == "#storage") {
            $("#storage").removeClass("hidden");
            $("#tab_name").html("Storage Device");
            $("#acc").removeClass("active");
            $("#desk").removeClass("active");
            $("#dev").addClass("active");
            $("#mon").removeClass("active");
            $("#net").removeClass("active");
            $("#print").removeClass("active");
            $("#game").removeClass("active");
            $("#soft").removeClass("active");
            $("#lap").removeClass("active");
        } else if (selector == "#monitors") {
           $("#monitors").removeClass("hidden");
            $("#tab_name").html("Monitors");
            $("#acc").removeClass("active");
            $("#desk").removeClass("active");
            $("#dev").removeClass("active");
            $("#mon").addClass("active");
            $("#net").removeClass("active");
            $("#print").removeClass("active");
            $("#game").removeClass("active");
            $("#soft").removeClass("active");
            $("#lap").removeClass("active");
        } else if (selector == "#network") {
            $("#network").removeClass("hidden");
            $("#tab_name").html("Networking");
            $("#acc").removeClass("active");
            $("#desk").removeClass("active");
            $("#dev").removeClass("active");
            $("#mon").removeClass("active");
            $("#net").addClass("active");
            $("#print").removeClass("active");
            $("#game").removeClass("active");
            $("#soft").removeClass("active");
            $("#lap").removeClass("active");
        } else if (selector == "#printer") {
            $("#printer").removeClass("hidden");
            $("#tab_name").html("Printer & Scanner");
            $("#acc").removeClass("active");
            $("#desk").removeClass("active");
            $("#dev").removeClass("active");
            $("#mon").removeClass("active");
            $("#net").removeClass("active");
            $("#print").addClass("active");
            $("#game").removeClass("active");
            $("#soft").removeClass("active");
            $("#lap").removeClass("active");
        }else if(selector == "#controller"){
            $("#controller").removeClass("hidden");
            $("#tab_name").html("Video Game Controller");
            $("#acc").removeClass("active");
            $("#desk").removeClass("active");
            $("#dev").removeClass("active");
            $("#mon").removeClass("active");
            $("#net").removeClass("active");
            $("#print").removeClass("active");
            $("#game").addClass("active");
            $("#soft").removeClass("active");
            $("#lap").removeClass("active");
        }else if(selector == "#software"){
            $("#software").removeClass("hidden");
            $("#tab_name").html("Software");
            $("#acc").removeClass("active");
            $("#desk").removeClass("active");
            $("#dev").removeClass("active");
            $("#mon").removeClass("active");
            $("#net").removeClass("active");
            $("#print").removeClass("active");
            $("#game").removeClass("active");
            $("#soft").addClass("active");
            $("#lap").removeClass("active");
        }else if(selector == "#other"){
            $("#other").removeClass("hidden");
            $("#tab_name").html("Other Computer & Laptop");
            $("#acc").removeClass("active");
            $("#desk").removeClass("active");
            $("#dev").removeClass("active");
            $("#mon").removeClass("active");
            $("#net").removeClass("active");
            $("#print").removeClass("active");
            $("#game").removeClass("active");
            $("#soft").removeClass("active");
            $("#lap").addClass("active");
        }else if(selector == "#desktop"){
            $("#desktop").removeClass("hidden");
            $("#tab_name").html("Desktop");
            $("#acc").removeClass("active");
            $("#desk").addClass("active");
            $("#dev").removeClass("active");
            $("#mon").removeClass("active");
            $("#net").removeClass("active");
            $("#print").removeClass("active");
            $("#game").removeClass("active");
            $("#soft").removeClass("active");
            $("#lap").removeClass("active");
        }
    });
}

function imageUploaded(){
    var aaa = $("#abc").val();
    var bbb = $("#blah").attr("src");



    var png = bbb.split(',')[1];

    alert(png);
    SDR.saveImage(png, saveImageCallback());
}
function saveImageCallback(){
    return function (status, data){
        alert("sucessful ");
    }
}
//function tecTabPane(){
//    $("#tec_name").html("Printing & Solution");
//    $("#solution").removeClass("hidden");
//    $('#items-tab a').tab('show');
//    $('#items-tab a').click(function (e) {
//        e.preventDefault();
//        $(this).tab('show');
//        var selector = $(this).attr('href');
//        if (selector == "#communication") {
//            $("#communication").removeClass("hidden");
//            $("#tec_name").html("Communication System");
//            $("#print").removeClass("active");
//            $("#sys").addClass("active");
//            $("#multi").removeClass("active");
//            $("#net").removeClass("active");
//            $("#train").removeClass("active");
//
//        } else if (selector == "#multimedia") {
//            $("#multimedia").removeClass("hidden");
//            $("#tec_name").html("Multimedia");
//            $("#print").removeClass("active");
//            $("#sys").removeClass("active");
//            $("#multi").addClass("active");
//            $("#net").removeClass("active");
//            $("#train").removeClass("active");
//
//        } else if (selector == "#networking") {
//            $("#networking").removeClass("hidden");
//            $("#tec_name").html("Networking Solution");
//            $("#print").removeClass("active");
//            $("#sys").removeClass("active");
//            $("#multi").removeClass("active");
//            $("#net").addClass("active");
//            $("#train").removeClass("active");
//
//        } else if (selector == "#training") {
//            $("#training").removeClass("hidden");
//            $("#tec_name").html("IT training");
//            $("#print").removeClass("active");
//            $("#sys").removeClass("active");
//            $("#multi").removeClass("active");
//            $("#net").removeClass("active");
//            $("#train").addClass("active");
//
//        }else{
//            $("#tec_name").html("Printing & Solution");
//            $("#solution").removeClass("hidden");
//            $("#print").addClass("active");
//            $("#sys").removeClass("active");
//            $("#multi").removeClass("active");
//            $("#net").removeClass("active");
//            $("#train").removeClass("active");
//        }
//    });
//}

//function repairTabPane(){
//    $("#repair_name").html("Computer & Laptop");
//    $("#computer").removeClass("hidden");
//    $('#items-tab a').tab('show');
//    $('#items-tab a').click(function (e) {
//        e.preventDefault();
//        $(this).tab('show');
//        var selector = $(this).attr('href');
//        if (selector == "#gaming") {
//            $("#gaming").removeClass("hidden");
//            $("#repair_name").html("Gaming Accessories");
//            $("#lap").removeClass("active");
//            $("#acc").addClass("active");
//            $("#oth").removeClass("active");
//
//        } else if (selector == "#other") {
//            $("#other").removeClass("hidden");
//            $("#repair_name").html("Other");
//            $("#lap").removeClass("active");
//            $("#acc").removeClass("active");
//            $("#oth").addClass("active");
//        } else {
//            $("#computer").removeClass("hidden");
//            $("#repair_name").html("Computer & Laptop");
//            $("#lap").addClass("active");
//            $("#acc").removeClass("active");
//            $("#oth").removeClass("active");
//        }
//    });
//}