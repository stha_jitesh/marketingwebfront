
function getProduct() {
    SDR.getProduct(getProductCallBack());
}

function getProductCallBack(){
    return function(status, data){
        var allProduct = "";
        // var a = "<div class='owl-carousel product_carousel_tab'>";
        for (index in data.data){
            var items = data.data[index];
            var discountP = items.discount;
            var oldPrice = items.price;
            var discount = (oldPrice * discountP)/100;
            var newPrice = oldPrice - discount;

            allProduct += "<li class='product col-md-3 sale'><a href='shop-simple-product.html'><span class='onsale'>Sale!</span>" +
                "<div class='product-image'>" +
                "<div class='product-image-front'><img width='700' height='893' src='assets/images/shop/skirt1-1-700x893.jpg' alt='skirt1-1'/></div>" +
                "<div class='product-image-back'><img src='assets/images/shop/skirt1-2-700x893.jpg' alt=''/></div>" +
                "<div class='product-image-overlay'><h4>View Details</h4><div class='star-rating' title='Rated 5.00 out of 5'>" +
                "<span style='width:100%'><strong class='rating'>5.00</strong> out of 5</span></div></div></div></a>" +
                "<div class='product-info'><h3 class='product-title'>" +
                "<a href='shop-simple-product.html'>"+items.name+"</a></h3>" +
                "<span class='product-categories'> <a href='shop-mens-category.html' rel='tag'>Jeans</a>,<a href='shop-mens-category.html' rel='tag'>Skirts</a>, <a href='shop-mens-category.html' rel='tag'>Tops</a>,<a href='shop-mens-category.html' rel='tag'>Womens</a></span>" +
                "<h3 class='price'><del><span class='amount'>&#36;"+oldPrice+"</span></del><ins><span class='amount'>&nbsp;&#36;"+newPrice+"</span></ins></h3>" +
                "<a href='#' rel='nofollow' class='add-to-cart-button add_to_cart_button product_type_simple'><i class='icon-bag'></i></a></div>" +
                "</li>"
        }

        $("#products").append(allProduct);

    }
}

function getAll(){
    SDR.getProduct(getAllCallback())
}
function getAllCallback(){
    return function(status, data){
        var allProduct;
        for (index in data.data){
            var items = data.data[index];
            allProduct += "<div class='product-thumb'><div class='image'><a href='product.html'><img src='image/product/samsung_tab_1-220x330.jpg' alt='Aspire Ultrabook Laptop' title='Aspire Ultrabook Laptop' class='img-responsive' /></a></div>" +
                "<div class='caption'><h4><a href='product.html'>"+items.name+"</a></h4></div></div>"
        }
        $("#place").html(allProduct);
    }
}

function addUser() {
    // var id = $("#identity").val();
    var userName = $("#username").val();
    var password = $("#password").val();
    var email = $("#email").val();
    var address = $("#address").val();
    var phone = $("#phone").val();
    // if (id==""){
        SDR.addUser(userName, password, email, phone, address,addUserCallBack())
    // }
    // else{
    //     SDR.updateUser(id, userName, password, email, phone, address,addUserCallBack())
    // }
}

function addUserCallBack() {
    return function(status, data){
        if (data.save=="success")
        alert ("hey")
    }
}

function addCategory(){
    var id = $("#id").val();
    var name = $("#CategoryName").val();
    if(id == null || id == "") {
        SDR.addCategory(name, addCategoryCallback())
    }
    else{
        SDR.editCategory(id, name, addCategoryCallback());
    }
}
function addCategoryCallback(){
            return function (status, data){
            if(data.save == "success" ){
                $("#id").val();
                $("#CategoryName").val("");
                getCategory();
            }
        };
}

function getCategory(){
    SDR.getCategory(getCategoryCallBack())
}
function getCategoryCallBack(){
    return function (status, data) {
        var sn = 1;
            tableData = [];
            for (index in data.data) {
                var sample = data.data[index];
                var id = sample.id;
                var name = "<a href='subCategory.html?id="+id+"'>"+sample.name+"</a>"
                var edit = "<button type='button' class='btn btn-info' onclick='editCategory(\"" + sample.id + "\",\"" + sample.name + "\")'>Edit</button>"+"<button type='button' class='btn btn-info' onclick='deleteCategory(\"" + sample.id + "\")'>Delete</button>"
                tableData.push([sn, name, edit])
            }
            sn++;
            createDataTable("categoryDisplay", tableData, 0, "desc")
        };
}

function editCategory(id, name){
    $("#id").val(id);
    $("#CategoryName").val(name);
}

function getSubCategory(){
    var id = getQueryParam("id");
    SDR.getSubCategory(id, getSubCategoryCallBack())
}
function getSubCategoryCallBack(){
    return function (status, data) {
        var sn = 1;
        tableData = [];
        for (index in data.data) {
            var sample = data.data[index];
            var id = sample.id;
            var name = "<a href='subCategory.html?id="+id+"'>"+sample.name+"</a>"
            var edit = "<button type='button' class='btn btn-info' onclick='editSubCategory(\"" + sample.id + "\",\"" + sample.name + "\")'>Edit</button>"+"<button type='button' class='btn btn-info' onclick='deleteSubCategory(\"" +id+ "\")'>Delete</button>"
            tableData.push([sn, name, edit])
        }
        sn++;
        createDataTable("subCategoryDisplay", tableData, 0, "asc")
    };
}
function editSubCategory(id, name) {
    $("#id").val(id);
    $("#subCategoryName").val(name);
};

function addSubCategory(){
    var id = $("#id").val();
    var cId = getQueryParam("id");
    var name = $("#subCategoryName").val();
    if(id == null || id == "") {
        SDR.addsubCategory(cId, name, addSubCategoryCallback())
    }
    else{
        SDR.editsubCategory(cId, id, name, addSubCategoryCallback());
    }
}
function addSubCategoryCallback(){
    return function (status, data){
        if(data.save == "success" ){
            $("#id").val("");
            $("#subCategoryName").val("");
            getSubCategory();
        }
    };
}
function deleteCategory(id){
    SDR.deleteCategory(id, deleteCategoryCallback())
}
function deleteCategoryCallback(){
    return function (status, data){
        if(data.save == "success"){
            getCategory();
        }
    }
}

function deleteSubCategory(id){
    SDR.deleteSubCategory(id, deleteSubCategoryCallback())
}
function deleteSubCategoryCallback(){
    return function (status, data){
        if(data.save == "success"){
            getSubCategory();
        }
    }
}