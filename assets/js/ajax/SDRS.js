/**
 * Created by sghimire on 7/24/2016.
 */
if (typeof SDR !== 'object') {
    var SDR = {};
}

(function () {

    // 'use strict';
    //
    // var requestHeaders = {"Accept": "application/json", "Content-Type": "application/json"};
    //
    // function request(url, request_type, datatype, callback, postdata) {
    //
    //     // For debug only
    //     //var SERVER = "http://192.168.10.20:8080";
    //     var SERVER = "http://localhost:8080";
    //     /*"http://52.27.46.230:8080";*/
    //     url = SERVER + url;
    //     console.log(url);
    //     if (typeof(postdata) == "object") {
    //         postdata = JSON.stringify(postdata);
    //
    //     }
    //
    //     $.ajax({
    //         beforeSend: function (xhr) {
    //             for (var key in requestHeaders) {
    //                 xhr.setRequestHeader(key, requestHeaders[key]);
    //                 ajaxindicatorstart("...Please wait.. Data is loading...");
    //             }
    //         },
    //         url: url,
    //         xhrFields: {
    //             withCredentials: true
    //         },
    //         data: postdata,
    //         dataType: datatype,
    //         type: request_type,
    //         success: function (data) {
    //             ajaxindicatorstop();
    //             callback("success", data);
    //         },
    //         error: function (xhr, status, error) {
    //             ajaxindicatorstop();
    //             try {
    //                 var errorMessageObj = JSON.parse(xhr.responseText);
    //             } catch (err) {
    //                 errorMessageObj = {type: "ERROR", message: xhr.responseText};
    //             }
    //             callback(error, errorMessageObj);
    //         }
    //     });
    // }


    SDR.getProduct = function(callback){
        var url= "/marketingback/product/getVisible";
        request(url,"GET","json",callback)
    };

    SDR.addUser=function (username, password, email, phone, address, callback) {
        var url="/marketingback/Userregiste/insertUpdateUsers"+
        "username="+username+"&"+
        "password="+password+"&"+
        "email="+email+"&"+
        "phone="+phone+"&"+
        "address="+address;
        request(url, "GET", "json", callback)
    };
    SDR.updateUser=function (username, password, email, phone, address, callback) {
        var url="/marketingback/Userregister/insertUpdateUsers"+
        request(url, "GET", "json", callback)
    };
    SDR.addCategory = function(name, callback){
        var url = "/marketingback/category/insertUpdate?name="+name;
        request(url, "GET", "json", callback);
    };
    SDR.getCategory = function(callback){
        var url = "/marketingback/category/getVisible";
        request(url, "GET", "json", callback);
    };
    SDR.editCategory = function(id, name, callback){
        var url = "/marketingback/category/insertUpdate?id="+id+"&name="+name;
        request(url, "GET", "json", callback)
    }

    SDR.getSubCategory = function(id, callback){
        var url = "/marketingback/subCategory/subCategorybyCategory?id="+id;
        request(url, "GET", "json", callback);
    };

    SDR.addsubCategory = function(cId, name, callback){
        var url = "/marketingback/subCategory/insertUpdate?cId="+cId+"&name="+name;
        request(url, "GET", "json", callback)
    };
    SDR.editsubCategory = function (cId, id, name, callback){
        var url = "/marketingback/subCategory/insertUpdate?cId="+cId+"&scId="+id+"&name="+name;
        request(url, "GET", "json", callback);
    }
    SDR.deleteCategory =  function(id, callback){
        var url = "/marketingback/category/deleteCategory?id="+id;
        request(url, "GET", "json", callback);
    };

    SDR.deleteSubCategory = function(id, callback){
        var url = "/marketingback/subCategory/deleteSubCategory?id="+id;
        request(url, "GET", "json", callback);
    }

}());