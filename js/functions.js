function include(url, container, selector){
	document.write(container);//creates the specified container in the DOM
	$(selector).load(url);//load the url content on the container
	console.log("adding "+selector);
}

function getQueryParam(name) {
	name = name.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
	var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
	results = regex.exec(location.search);
	return results == null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
}

function copyTable(original,copyTo) {
    var source = document.getElementById(original);
    var destination = document.getElementById(copyTo);
    var copy = source.cloneNode(true);
    copy.setAttribute('id', 'tempTable');
    copy.setAttribute('class','hidden');
    destination.parentNode.replaceChild(copy, destination);
}

function createDataTable(id, data, colIndex, sortOrder,displayLength){
	//by default, sort first index column data in ASC order
	colIndex = colIndex==null? 0 : colIndex;
	sortOrder = sortOrder==null? asc : sortOrder;

    displayLength = displayLength==null? 5: displayLength;
	
	var oTable = $("#"+id).dataTable({
		"iDisplayLength": displayLength,
    	"aLengthMenu": [[5, 10, 25, 50, -1], [5, 10, 25, 50, "All"]],
    	"bDestroy": true
	});
	oTable.fnClearTable();
	oTable.fnSort([[ colIndex, sortOrder]]);
	oTable.fnAddData(data);
	oTable.fnDraw();
	return oTable;
}

function vaildateForm(fieldSelectors, successCallback){
	$(function () { $(fieldSelectors).jqBootstrapValidation({
        preventSubmit: true,
        submitSuccess: function($form, event) {
            event.preventDefault();
            successCallback();
        },
        submitError: function($form, event, errors){
    		for(var errElement in errors){
		      if(errElement=='undefined') continue;

		      $("*[name="+errElement+"]").addClass("err-border").focus(function(){
		          $(this).removeClass("err-border");
		      });
		  	}
        }


      }
    ); 
  });
}
function ajaxindicatorstart(text)
{
    if(jQuery('body').find('#resultLoading').attr('id') != 'resultLoading'){
        jQuery('body').append('<div id="resultLoading" style="display:none"><div><img src="../img/1.gif"><div>'+text+'</div></div><div class="bg"></div></div>');
    }

    jQuery('#resultLoading').css({
        'width':'100%',
        'height':'100%',
        'position':'fixed',
        'z-index':'10000000',
        'top':'0',
        'left':'0',
        'right':'0',
        'bottom':'0',
        'margin':'auto'
    });

    jQuery('#resultLoading .bg').css({
        'background':'#000000',
        'opacity':'0.7',
        'width':'100%',
        'height':'100%',
        'position':'absolute',
        'top':'0'
    });

    jQuery('#resultLoading>div:first').css({
        'width': '250px',
        'height':'75px',
        'text-align': 'center',
        'position': 'fixed',
        'top':'0',
        'left':'0',
        'right':'0',
        'bottom':'0',
        'margin':'auto',
        'font-size':'16px',
        'z-index':'10',
        'color':'#ffffff'

    });

    jQuery('#resultLoading .bg').height('100%');
    jQuery('#resultLoading').fadeIn(300);
    jQuery('body').css('cursor', 'wait');
}

function ajaxindicatorstop()
{
    jQuery('#resultLoading .bg').height('100%');
    jQuery('#resultLoading').fadeOut(300);
    jQuery('body').css('cursor', 'default');
}